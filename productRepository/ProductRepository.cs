﻿using Assignment_3.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Assignment_3.productRepository
{
    internal class ProductRepository
    {
        // class arrAY
        Product[]  Products;

        public ProductRepository()
        {
            Products = new Product[]
            {
            new Product(1, "Realme", 23000, "Mobile"),
            new Product(2, "Oppo", 12000, "Mobile"),
            new Product(3, "Techno", 22000, "Mobile"),
            new Product(4, "coolar", 8000, "Electrical"),
            new Product(5, "Washing machine", 28000, "Electrical")
            };

        }
        public Product[] GetAllProduct()
        {
            return Products;
        }
     
        // get product by id
        public Product GetProductById(int id)
        {
            Product  productobj = null;
            foreach (Product item in Products)
            {
                if(item.Id == id)
                {
                    productobj = item;
                }
                else
                {
                    continue;
                }

            }
            return  productobj;
        }
        // Get product category

        public void GetProductByCategory(string category)
        {
           var  result = Array.FindAll(Products, x => x.Category == category);  
            foreach (Product item in result)
            {
                Console.WriteLine($"ID :{item.Id}  Name ::{item.Name} Price::{item.Price} Category ::{item.Category}");
            }
            
        }

            
    }
}
